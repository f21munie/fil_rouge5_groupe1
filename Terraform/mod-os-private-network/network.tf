resource "openstack_networking_network_v2" "internal_net" {
  name           = var.NETWORK_NAME
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "internal_subnet" {
  name       = var.SUBNET_NAME
  network_id = "${openstack_networking_network_v2.internal_net.id}"
  cidr       = var.SUBNET_IP_RANGE
  ip_version = 4
  dns_nameservers = var.DNS
}
