# Floating ip pour bastion
resource "openstack_networking_floatingip_v2" "floatip_admin" {
  pool        = "external"
  description = "Floating IP pour l'instance ${var.INSTANCE_BASTION_NAME}"

  # Copie IP publique dans un fichier .txt sur la machine hébergeante
  provisioner "local-exec" {
    command = "mkdir ip_files; echo '${var.INSTANCE_BASTION_NAME} : ${self.address}' > ip_files/public_ips.txt"
  
  }
}

# Rattachement de l'ip flottante à l'intance bastion
resource "openstack_compute_floatingip_associate_v2" "fip_admin" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_admin.address}"
  instance_id = "${openstack_compute_instance_v2.bastion_instance.id}"

  depends_on = [openstack_networking_floatingip_v2.floatip_admin]
}

# Floating ip pour node01
resource "openstack_networking_floatingip_v2" "floatip_application" {
  pool        = "external"
  description = "Floating IP pour l'instance ${var.INSTANCE_ORCHEST_NAME[0]}"

  # Copie IP publique dans un fichier .txt sur la machine hébergeante
  provisioner "local-exec" {
    command = "echo '${var.INSTANCE_ORCHEST_NAME[0]} : ${self.address}' >> ip_files/public_ips.txt"
  }

  depends_on = [openstack_networking_floatingip_v2.floatip_admin]
}

# Rattachement de l'ip flottante à l'intance node01
resource "openstack_compute_floatingip_associate_v2" "fip_application" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_application.address}"
  instance_id = "${openstack_compute_instance_v2.orchestration_instance["${var.INSTANCE_ORCHEST_NAME[0]}"].id}"

  depends_on = [openstack_networking_floatingip_v2.floatip_application]
}
