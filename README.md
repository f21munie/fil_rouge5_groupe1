# Descriptif du projet

- Auteur : Florian MUNIER, Schadrac NOUFE SIE, Marie Tiffany NGONO NSA, Ousmane NGOUYAMSA NDAM
- Formation : MS Infrastructures Cloud et DevOps
- Date : 13/03/2023 - 16/03/2023
- Description : Ce projet a pour but de déployer les outils de monitoring Prométheus et Zabbix.

> A noter qu'il faut au préalable avoir créé l'infrastructure via Terraform (lien du GitLab ci-dessus) et lancer le cluster Kubernetes.

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/fil_rouge5_groupe1.git
```

#######################################################################################################
###################################CONFIGURER TERRAFORM###############################################
#######################################################################################################

# Installation de Terraform

- Installer Terraform :

```sh
TF_VERSION=1.3.6

echo "Downloading terraform binary ..."
if [ ! -e terraform_${TF_VERSION}_linux_amd64.zip ]; then  
  curl -O https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip
fi
if [ ! -x ~/bin/terraform ]; then 
  unzip terraform_${TF_VERSION}_linux_amd64.zip -d ~/bin/
fi
rm -f terraform_${TF_VERSION}_linux_amd64.zip
```

- Vérifier le bon fonctionnement de Terraform :

```sh
terraform -v 
```

> Terraform v1.3.6
> on linux_amd64

# Configuration de l'interaction avec Openstack

Pour pouvoir interagir avec Openstack, créer ou récupérer via horizon un fichier RC, et initialiser l'environnement :

```sh
cd projet_terraform_ansible_k8s
source os-openrc.sh
env | grep OS_
```
> Le fichier "os-openrc.sh" correspond à votre fichier RC perso. N'oubliez pas de préciser le chemin.

# Renseigner les variables de Terraform

Il est nécessaire de paramétrer les variables suivantes afin de choisir vos caractéristiques de l'infrastructure (, nom du réseau, du sous-réseau, du routeur, le CIDR, ...). 
Pour cela, allez dans le fichier "main.tf" et veuillez à renseigner les variables. Vous pouvez modifier les variables à partir <ROUTER_NAME> jusqu'à <K3S_TOKEN>.

```sh
cd projet_terraform_ansible_k8s/Terraform
sudo vim main.tf
```

```sh
module "mod-os-private-network" {
  source                           = "./mod-os-private-network/"
  KEYPAIR_PATH                     = "$HOME/.ssh"
  EXTERNAL_NETWORK                 = "external"
  ROUTER_NAME                      = "router_tf_private"
  NETWORK_NAME                     = "network_tf_private"
  SUBNET_NAME                      = "subnet_tf_private"
  SUBNET_IP_RANGE                  = "192.168.1.0/24"
  DNS                              = ["192.44.75.10"]
  INSTANCE_BASTION_NAME            = "bastion"
  INSTANCE_BASTION_IMAGE           = "imta-docker"
  INSTANCE_BASTION_FLAVOR          = "s10.medium"
  INSTANCE_BASTION_KEY_PAIR        = "projet_terraform"
  INSTANCE_ORCHEST_NAME            = ["node01", "node02", "node03"]
  INSTANCE_ORCHEST_IMAGE           = "imta-docker"
  INSTANCE_ORCHEST_FLAVOR          = "s10.medium"
  INSTANCE_ORCHEST_KEY_PAIR        = "cluster_key"
  INSTANCE_ZABBIX_SERVER_NAME      = "zabbix-server"
  INSTANCE_ZABBIX_SERVER_IMAGE     = "imta-docker"
  INSTANCE_ZABBIX_SERVER_FLAVOR    = "s10.medium"
  INSTANCE_ZABBIX_SERVER_KEY_PAIR  = "projet_terraform"
  SECGROUP_BASTION_NAME            = "secgroup_bastion"
  SECGROUP_APPLICATION_NAME        = "secgroup_application"
  SECGROUP_INTERNAL_NETWORK_NAME   = "secgroup_internal_network"
  K3S_TOKEN                        = "cluster"
}
```

#######################################################################################################
#####################################CONFIGURER ANSIBLE################################################
#######################################################################################################

# Création d'un environnement virtuel

```sh
cd projet_terraform_ansible_k8s
python3 -m venv ./venv/ansible
source ./venv/ansible/bin/activate
pip install ansible
deactivate
```
#######################################################################################################
########################################DEPLOIEMENT###################################################
#######################################################################################################


# Déploiement de l'architecture et de l'application Vapormap

```sh
cd projet_terraform_ansible_k8s
source ./venv/ansible/bin/activate
cd projet_terraform_ansible_k8s/Terraform
terraform init
terraform plan
terraform apply
```
> Terraform apply vous demandera d'entrer une valeur (Enter a value:), taper "yes" et valider avec "entrée".

> L'application Vapormap est automatiquement déployée par Terraform qui lance le playbook Ansible.

# Accéder à l'application

Pour accéder à l'application Vapormap, rendez-vous sur les adresses suivantes :

> Vous pouvez trouver la variable <PUB_API_IP> dans le fichier "hosts.ini".

- Frontend

```sh
http://<PUB_API_IP>
```

- API

```sh
http://<PUB_API_IP>/api/points/
```

# Visualisation de l'architecture

Vous pouvez vérifier l'état des ressources vues par Terraform. Elles seront listées avec leurs attribus.

```sh
terraform show
```
Vous pouvez également lister les ressources par type dans Openstack, directement en ligne de commande :

```sh
openstack network list 
openstack subnet  list
openstack router list 
openstack floating ip list
openstack security group list 
```

L'architecture est graphiquement visualisable via Openstack :

```sh
Projet / Réseau / Topologie du réseau
```

# Suppression de l'application Vapormap sur une instance

```sh
cd projet_terraform_ansible_k8s
source ./venv/ansible/bin/activate
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i Ansible/hosts.ini Ansible/destroy.yml
deactivate
```

# Suppression de l'architecture

Si vous voulez supprimer l'architecture, lancer la commande suivante :

```sh
cd projet_terraform_ansible_k8s/Terraform
terraform destroy
```

#######################################################################################################
########################################MONITORING####################################################
#######################################################################################################

# Déploiement de l'outil Prométheus


# Déploiement de l'outil zabbix

1 - Se connecter au serveur Zabbix : 

```sh
http://<IP_PUBLIC_ZABBIX_SERVER>/zabbix
```

> Identifiants : Admin / zabbix

2 - Création d'un groupe

```sh
- Sélectionner "Collecte de données"
- Sélectionner "Groupes d'hôtes"
- Sélectionner "Créer un groupe d'hôtes"
- Remplir le champ demandé (ex : VaporMap)
- Sélectionner "Ajouter"
```

3 - Création des hôtes

```sh
- Sélectionner "Collecte de données"
- Sélectionner "hôtes"
- Sélectionner "Créer un hôte"
- Remplir les champs demandés en indiquant
    - les noms des hôtes (node01, node02, node03, bastion)
    - les modèles (	Linux by Zabbix agent active)
    - les interfaces IPMI (IP privée de chaque hôte)
- Sélectionner "Ajouter"
```

4 - Visualiser les métrics

```sh
Pour visualiser de façon générale les alertes :
- Sélectionner "Tableaux de bord"

Pour avoir le détail des métrics :
- Sélectionner "Surveillance"
- Sélectionner "Dernière données"
```
